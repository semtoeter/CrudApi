﻿using CrudApi.Data;
using CrudApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CrudApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly CrudApiProductsdb dbContext;

        public ProductsController(CrudApiProductsdb dbContext)
        {
            this.dbContext = dbContext;
        }
        [HttpGet]
        public IActionResult GetAllProducts()
        {
            return Ok(dbContext.Products.ToList());
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetSpecificProduct([FromRoute] Guid id)
        {
            var product = await dbContext.Products.FindAsync(id);
            return Ok(product);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(AddProduct addProduct)
        {
            Product product = new Product()
            {
                Id = Guid.NewGuid(),
                Sku = addProduct.Sku,
                Name = addProduct.Name,
                Price = addProduct.Price
            };

            await dbContext.Products.AddAsync(product);
            await dbContext.SaveChangesAsync();

            return Ok(product);
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateProduct([FromRoute] Guid id, UpdateProduct updateProduct)
        {
            var product = await dbContext.Products.FindAsync(id);

            if (product != null)
            {
                product.Sku = updateProduct.Sku;
                product.Name = updateProduct.Name;
                product.Price = updateProduct.Price;

                await dbContext.SaveChangesAsync();

                return Ok(product);
            }

            return NotFound();
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async void RemoveProduct([FromRoute] Guid id)
        {
            var product = await dbContext.Products.FindAsync(id);

            if (product != null)
            {
                dbContext.Remove(product);
                dbContext.SaveChanges();
            }
        }
    }
}
