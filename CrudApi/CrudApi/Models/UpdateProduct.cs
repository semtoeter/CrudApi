﻿namespace CrudApi.Models
{
    public class UpdateProduct
    {
        public int Sku { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
