﻿namespace CrudApi.Models
{
    public class AddProduct
    {
        public int Sku { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
